import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    noteId: 1,
    notesList: [],
    currentNote: {
      id: 1,
      title: '',
      list: [
        {
          isDone: false,
          text: "",
          id: 1
        }
      ]
    },
    currentListId: 1
  },
  mutations: {
    addNote(state, note) {
      const findNote = state.notesList.find(n => n.id === note.id);
      console.log("findNote: ", findNote);
      if (findNote) {
        const indexNode = state.notesList.indexOf(findNote);
        state.notesList.splice(indexNode, 1, note);
      } else {
        state.notesList.push(note);
      }
    },
    incrementNoteId: state => {
      const item = state.notesList.find(note => note.id === state.noteId);
      if (item) {
        state.noteId++;
      }
    },
    removeNote(state, noteId) {
      const indexNote = state.notesList.findIndex(note => note.id === noteId);
      indexNote >= 0 && state.notesList.splice(indexNote, 1);
    },
    setCurrentNote(state, noteId) {
      const indexNote = state.notesList.findIndex(note => note.id === noteId);
      indexNote >= 0 && (state.currentNote = state.notesList[indexNote]);
    }
  },
  actions: {},
  modules: {}
});
