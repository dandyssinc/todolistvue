import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  beforeCreate() {
    // запускаем мутацию для вытягивания данных из localstorage
    this.$store.commit("initialiseStore");
  },
  mounted() {
    // для записи state в localStorage
    this.$store.subscribe((mutation, state) => {
      localStorage.setItem("store", JSON.stringify(state));
    });
  },
  render: h => h(App)
}).$mount("#app");
