import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Detail from "../views/Detail.vue";

Vue.use(VueRouter);

// Директива для автофокуса на поле.
Vue.directive("focus", {
  // Когда привязанный элемент вставлен в DOM...
  inserted: function(el) {
    // Переключаем фокус на элемент
    el.focus();
  }
});

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/detail",
    name: "Detail",
    component: Detail
    // component: () => import("../views/Detail.vue")
  },
  {
    path: "*",
    name: "NotFound",
    component: Home
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
