import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const initCurrentNote = {
  id: 1,
  title: "",
  init: true,
  list: [
    {
      isDone: false,
      text: "",
      id: 1
    }
  ]
};

export default new Vuex.Store({
  state: {
    noteId: 1,
    notesList: [],
    currentNote: initCurrentNote,
    currentListId: 1,
    draftCurrentNote: null
  },
  mutations: {
    /**
     * Мутация для установки state из localStorage
     */
    initialiseStore(state) {
      if (localStorage.getItem("store")) {
        this.replaceState(
          Object.assign(state, JSON.parse(localStorage.getItem("store")))
        );
      }
    },

    /**
     * Добавление заметки
     * @param note Заметка.
     */
    addNote(state, note) {
      const findNote = state.notesList.find(n => n.id === note.id);
      if (findNote) {
        const indexNode = state.notesList.indexOf(findNote);
        state.notesList.splice(indexNode, 1, note);
      } else {
        state.notesList.push(note);
      }
      state.draftCurrentNote = null;
    },

    /**
     * Очищение черновика.
     * @param state
     */
    clearDraft(state) {
      state.draftCurrentNote = null;
    },

    /**
     * Инкремент id Заметки.
     */
    incrementNoteId: state => {
      const item = state.notesList.find(note => note.id === state.noteId);
      if (item) {
        state.noteId++;
      }
    },

    /**
     * Очистка заметки.
     * @param state
     */
    resetCurrentNote(state) {
      state.currentNote = initCurrentNote;
      state.currentListId = 1;
    },

    /**
     * Удаление заметки.
     * @param noteId id Заметки.
     */
    removeNote(state, noteId) {
      const indexNote = state.notesList.findIndex(note => note.id === noteId);
      indexNote >= 0 && state.notesList.splice(indexNote, 1);
    },

    /**
     * Установка выбранной заметки для редактирования.
     * @param noteId id Заметки.
     */
    setCurrentNote(state, noteId) {
      const indexNote = state.notesList.findIndex(note => note.id === noteId);
      if (indexNote >= 0) {
        state.currentNote = state.notesList[indexNote];
        state.currentListId =
          state.currentNote.list[state.currentNote.list.length - 1].id;
      }
    },

    /**
     * Инкремент id элемента списка задач.
     */
    incrementCurrentListId: state => ++state.currentListId,

    /**
     * Установка черновика текущей заявки.
     * @param note Заявка.
     */
    setDraftCurrentNote(state, note) {
      state.draftCurrentNote = note;
    },

    /**
     * Применение черновика.
     */
    applyDraft(state) {
      if (!state.draftCurrentNote) return;
      state.currentNote = Object.assign({}, state.draftCurrentNote);
      state.currentListId =
        state.currentNote.list[state.currentNote.list.length - 1].id;
    }
  },
  actions: {
    /**
     * Экшен для создания новой заявки.
     */
    createNewNote({ commit }) {
      commit("incrementNoteId");
      commit("resetCurrentNote");
    }
  }
});
